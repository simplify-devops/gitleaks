# gitleaks helpers 2020-11-06 by @k33g | on gitlab.com 
FROM zricethezav/gitleaks
LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

RUN apk --update add --no-cache nodejs

COPY analyze.js /usr/local/bin/analyze

RUN chmod +x /usr/local/bin/analyze

CMD ["/bin/sh"]
