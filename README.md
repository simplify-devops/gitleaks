# gitleaks

See:
- https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/Secret-Detection.gitlab-ci.yml
- https://docs.gitlab.com/ee/user/application_security/secret_detection/
- https://github.com/zricethezav/gitleaks

Gitleaks is a SAST tool for detecting hardcoded secrets like passwords, api keys, and tokens in git repos. Gitleaks aims to be the easy-to-use, all-in-one solution for finding secrets, past or present, in your code.


## Use it

```yaml
stages:
  - 🔎secure

include:
  - project: 'simplify-devops/gitleaks'
    file: 'gitleaks.gitlab-ci.yml'

👮‍♀️:secrets:detection:
  stage: 🔎secure
  extends: .gitleaks:analyzer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
```
