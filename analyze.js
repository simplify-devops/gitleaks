#!/usr/bin/env node

const fs = require("fs")
const { exec } = require("child_process")

console.log("👮‍♀️ secret detection")

let cmdArgs = process.argv.slice(2);
let directory = cmdArgs[0]

let start = new Date()

let severity = "Critical"
let confidence = "Unknown"
let scannerName = "Gitleaks"
let scannerId = "gitleaks"
let scannerSrc = "https://bots.garden"
let scannerVendor = "Bots.Garden"
let scannerVersion = "0.0.1"

//let cmd = `gitleaks --repo-path=${directory} --verbose --pretty --depth=1 --report=gitleaks.json`
//let cmd = `gitleaks --repo-path=${directory} --verbose --pretty --report=gitleaks.json`
let cmd = `gitleaks --path=${directory} --verbose --report=gitleaks.json`

exec(cmd, (error, stdout, stderr) => {

    if (error) {
        console.log(`😡 secret(s) detected: ${error.message}`)
        
        let gitleaksReport = fs.readFileSync("./gitleaks.json", "utf8")
        let gitleaksResults = JSON.parse(gitleaksReport)
        
        console.log(gitleaksResults)

        // ========== Helpers ==========
        let btoa = (string) => Buffer.from(string).toString("base64")

        let get_cve_id = (file, description, tags) => `${file}:${btoa(description)}:${tags.split(",").map(item => item.trim()).join(":")}`

        let get_id = (description, path, line) => `${btoa(description)}${btoa(path)}${btoa(line)}`

        let get_scanner = () => {
            return {
                id: scannerId, name: scannerName
            }
        }

        // ========== End of Helpers ==========

        console.log("📝 gitleaks report:")
        console.log(`------------------------------------------------------------`)
        gitleaksResults.forEach(item => {
        console.log(` 🔴 Rule:     ${item.rule}`)
        console.log(`    Commit:   ${item.commit}`)
        console.log(`    file:     ${item.file}`)
        console.log(`    line:     ${item.lineNumber}`)
        console.log(`------------------------------------------------------------`)
        })
        

        /* --- generate gl-secret-detection-report.json --- */
        let vulnerabilities = gitleaksResults
            .map(item => {
                return {
                    id:get_id(item.offender, item.file, ""+item.lineNumber),
                    category: "secret_detection",
                    name: `🔴: ${item.rule}`,
                    message: `📝: ${item.rule}`,
                    description: `🖐️: ${item.offender}`,
                    cve: get_cve_id(item.file, item.offender, item.tags),
                    severity: severity,
                    confidence: confidence,
                    scanner: get_scanner(),
                    location: {
                      file: item.file, 
                      start_line: item.lineNumber,
                      end_line: item.lineNumber,
                      commit: {
                        author: item.author,
                        message: item.commitMessage,
                        date: item.date,
                        sha: item.commit
                      }
                    },
                    identifiers: [
                      {
                        type: "gitleaks_rule_id",
                        name: `Gitleaks rule ${item.rule}`,
                        value: item.tags
                      }
                    ]                       
                } // return
            })

        let end = new Date() - start

        let secretReport = {
          version: "3.0",
          vulnerabilities: vulnerabilities,
          remediations: [],
          scan: {
            scanner: {
              id: scannerId,
              name: scannerName,
              url: scannerSrc,
              vendor: {
                name: scannerVendor
              },
              version: scannerVersion
            },
            type: "secret_detection",
            start_time: start,
            end_time: end,
            status: "success" // TODO try catch
          }
        }

        fs.writeFileSync("./gl-secret-detection-report.json", JSON.stringify(secretReport, null, 2))
        
        return
    } else {
        fs.writeFileSync("./gl-secret-detection-report.json", "[]")
    }

    if (stderr) {
        console.log(`stderr: ${stderr}`)
        return
    }
    console.log(`stdout: ${stdout}`)
})

